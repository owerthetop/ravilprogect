<?php

use yii\db\Migration;

/**
 * Class m180606_115909_later
 */
class m180606_115909_later extends Migration
{
    public $tableName = '{{%queue}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, 'timeout', $this->integer()->defaultValue(0)->notNull()->after('created_at'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'timeout');
    }
}
