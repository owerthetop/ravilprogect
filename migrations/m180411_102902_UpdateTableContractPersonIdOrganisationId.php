<?php

use yii\db\Migration;

/**
 * Class m180411_102902_UpdateTableContractPersonIdOrganisationId
 */
class m180411_102902_UpdateTableContractPersonIdOrganisationId extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE public.contract DROP CONSTRAINT "contract_organisationId_key"');
        $this->execute('ALTER TABLE public.contract DROP CONSTRAINT "contract_personId_key"');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }

}
