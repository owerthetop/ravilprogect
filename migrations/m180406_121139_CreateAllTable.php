<?php

use yii\db\Migration;

/**
 * Class m180406_121139_CreateAllTable
 */
class m180406_121139_CreateAllTable extends Migration
{
    public function safeUp()
    {
        $this->createTable('customer', [
            'id' => 'SERIAL',
            'organisationId' => $this->integer()->unique(),
            'personId' => $this->integer()->unique(),
            'name' => $this->string(),
            'number' => $this->string(500),
            'countProductPaid' => $this->integer()
        ]);

        $this->createTable('organisation', [
            'id' => 'SERIAL',
            'name' => $this->string(),
            'dateCreate' => $this->timestamp(),
            'dateUpdate' => $this->timestamp(),
            'inn' => $this->string()
        ]);

        $this->createTable('person', [
            'id' => 'SERIAL',
            'surname' => $this->string(),
            'name' => $this->string(),
            'patronymic' => $this->string(),
            'dateCreate' => $this->timestamp(),
            'dateUpdate' => $this->timestamp(),
            'inn' => $this->string()
        ]);


        $this->createTable('contract', [
            'id' => 'SERIAL',
            'personId' => $this->integer()->unique(),
            'organisationId' => $this->integer()->unique(),
            'number' => $this->string(500),
            'paid' => $this->boolean()
        ]);

        $this->createTable('product',[
            'id' => 'SERIAL',
            'contractId' => $this->integer(),
            'nameProduct' => $this->string()
        ]);

        $this->addPrimaryKey('customerPk', 'customer', 'id');
        $this->addPrimaryKey('organisationPk', 'organisation', 'id');
        $this->addPrimaryKey('personPk', 'person', 'id');
        $this->addPrimaryKey('contractPk', 'contract', 'id');
        $this->addPrimaryKey('productPk', 'product', 'id');

        $this->addForeignKey('organisationId', 'customer', 'organisationId', 'organisation', 'id');
        $this->addForeignKey('personId', 'customer', 'personId', 'person', 'id');
        $this->addForeignKey('contractIdPerson', 'contract', 'personId', 'person', 'id');
        $this->addForeignKey('contractIdOrganisation', 'contract', 'organisationId', 'organisation', 'id');
        $this->addForeignKey('product', 'product', 'contractId', 'contract', 'id');

    }

    public function safeDown()
    {
        $this->dropForeignKey('organisationId', 'customer');
        $this->dropForeignKey('personId', 'customer');
        $this->dropForeignKey('contractIdPerson', 'contract');
        $this->dropForeignKey('contractIdOrganisation', 'contract');
        $this->dropForeignKey('product', 'product');

        $this->dropTable('customer');
        $this->dropTable('organisation');
        $this->dropTable('person');
        $this->dropTable('contract');
        $this->dropTable('product');
    }
}
