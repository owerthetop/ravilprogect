<?php

use yii\db\Migration;

/**
 * Class m180412_114304_AddIndexCustomerAndRemoveIndexOrganisationPerson
 */
class m180412_114304_AddIndexCustomerAndRemoveIndexOrganisationPerson extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropIndex('nameOrganisation', 'organisation');
        $this->dropIndex('namePerson', 'person');
        $this->dropIndex('numberContract', 'contract');
        $this->createIndex('nameCustomer', 'customer', 'name');
        $this->createIndex('numberCustomer', 'customer', 'number');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createIndex('nameOrganisation', 'organisation', 'name');
        $this->createIndex('namePerson', 'person', 'name');
        $this->createIndex('numberContract', 'contract', 'number');
        $this->dropIndex('nameCustomer', 'customer');
        $this->dropIndex('numberCustomer', 'customer');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180412_114304_AddIndexCustomerAndRemoveIndexOrganisationPerson cannot be reverted.\n";

        return false;
    }
    */
}
