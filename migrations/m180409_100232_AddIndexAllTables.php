<?php

use yii\db\Migration;

/**
 * Class m180409_100232_AddIndexAllTables
 */
class m180409_100232_AddIndexAllTables extends Migration
{
    public function safeUp()
    {
        $this->createIndex('nameOrganisation', 'organisation', 'name');
        $this->createIndex('namePerson', 'person', 'name');
        $this->createIndex('numberContract', 'contract', 'number');
    }

    public function safeDown()
    {
        $this->dropIndex('nameOrganisation', 'organisation');
        $this->dropIndex('namePerson', 'person');
        $this->dropIndex('numberContract', 'contract');
    }

}
