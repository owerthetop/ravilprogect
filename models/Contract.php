<?php

namespace app\models;

use app\components\CustomerSyncBehavior;
use yii\db\ActiveRecord;

/**
 * Class Contract
 * @package app\models
 * @property integer personId
 * @property integer organisationId
 * @property string number
 * @property boolean paid
 */
class Contract extends ActiveRecord
{
    public function attributeLabels()
    {
        return [
            'organisationId' => 'ID организации',
            'personId' => 'ID физ лица',
            'number' => 'Номер договора',
            'paid' => 'Оплаченные продукты'
        ];
    }

    public function rules()
    {
        return [
            [['number'], 'string'],
            [['paid'], 'boolean']
        ];
    }

    public function behaviors()
    {
        return [
            CustomerSyncBehavior::class
        ];
    }

    public function fields()
    {
        return [
            'id',
            'number',
            'paid',
            'products'
        ];
    }

    public function getProducts ()
    {
        return $this->hasMany(Product::class, ['contractId' => 'id']);
    }
}