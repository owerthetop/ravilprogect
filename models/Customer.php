<?php

namespace app\models;

use yii\db\ActiveRecord;
/**
 * Class Customer
 * @package app\models
 * @property integer organisationId
 * @property  integer personId
 * @property string name
 * @property string number
 * @property integer countProductPaid
 */
class Customer extends ActiveRecord
{
    public function attributeLabels()
    {
        return [
            'organisationId' => 'Введите id юр лица!',
            'personId' => 'Введите id физ лица!',
            'name' => 'Введите имя',
            'number' => 'Введите номер договора!',
            'countProductPaid' => 'Введите число!'
        ];
    }

    public function rules()
    {
        return [
            [['name', 'number'], 'string'],
            [['countProductPaid', 'organisationId'], 'integer']
        ];
    }

    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        if ($this->personId){
            $data = $this->person->toArray();
            $data['id'] = $this->id;
        }

        if ($this->organisationId){
            $data = $this->organisation->toArray();
            $data['id'] = $this->id;
        }

        return $data;
    }

    public function getPerson()
    {
        return $this->hasOne(Person::class, ['id' => 'personId']);
    }

    public function getOrganisation ()
    {
        return $this->hasOne(Organisation::class, ['id' => 'organisationId']);
    }
}
