<?php

namespace app\models;

use app\components\CustomerSyncBehavior;
use yii\db\ActiveRecord;

/**
 * Class Organisation
 * @package app\models
 * @property Contract[] contracts
 * @property string name
 * @property string dateCreate
 * @property string dateUpdate
 * @property integer inn
 */
class Organisation extends ActiveRecord
{
    const VIEW_TYPE = 'organisation';

    public function attributeLabels()
    {
        return [
            'name' => 'Имя организации',
            'inn' => 'ИНН'
        ];
    }

    public function rules()
    {
        return [
            [['name', 'dateCreate', 'dateUpdate'], 'string'],
            [['inn'], 'integer']
        ];
    }

    public function behaviors()
    {
        return [
            CustomerSyncBehavior::class
        ];
    }

    public function fields()
    {
        return [
            'id',
            'name',
            'create',
            'update',
            'inn',
            'contracts',
            'modelName',
            'countProductPaid'
        ];
    }

    public function getModelName()
    {
        return  self::VIEW_TYPE;
    }

    public function getContracts ()
    {
        return $this->hasMany(Contract::class, ['organisationId' => 'id']);
    }

    public function getCreate ()
    {
        return strtotime($this->dateCreate);
    }

    public function getUpdate ()
    {
        return strtotime($this->dateUpdate);
    }

    public function getCustomer ()
    {
        return $this->hasOne(Customer::class, ['organisationId' => 'id']);
    }

    public function getCountProductPaid ()
    {
        return $this->customer->countProductPaid;
    }
}
