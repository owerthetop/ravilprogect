<?php
namespace app\models;

use app\components\CustomerSyncBehavior;
use yii\db\ActiveRecord;

/**
 * Class Product
 * @package app\models
 * @property Contract contract
 * @property integer contractId
 * @property string nameProduct
 */
class Product extends ActiveRecord
{
    public function attributeLabels()
    {
        return [
            'contractId' => 'ID контракта',
            'nameProduct' => 'Наименование продукта'
        ];
    }

    public function rules()
    {
        return [
            ['nameProduct', 'string'],
            ['contractId', 'integer']
        ];
    }

    public function behaviors()
    {
        return [
            CustomerSyncBehavior::class
        ];
    }

    public function fields()
    {
        return [
            'id',
            'nameProduct'
        ];
    }
}