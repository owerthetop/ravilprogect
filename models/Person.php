<?php

namespace app\models;

use app\components\CustomerSyncBehavior;
use yii\db\ActiveRecord;

/**
 * Class Person
 * @package app\models
 * @property string surname
 * @property string name
 * @property string patronymic
 * @property string dateCreate
 * @property string dateUpdate
 * @property integer inn
 */
class Person extends ActiveRecord
{
    const VIEW_TYPE = 'person' ;

    public function attributeLabels()
    {
        return [
            'surname' => 'Фамилия',
            'name' => 'Имя',
            'patronymic' => 'Отчество',
            'inn' => 'ИНН'
        ];
    }

    public function rules()
    {
        return [
            [['surname', 'name', 'patronymic', 'dateCreate', 'dateUpdate'], 'string'],
            [['inn'], 'integer']
        ];
    }

    public function behaviors()
    {
        return [
            CustomerSyncBehavior::class
        ];
    }

    public function fields()
    {
        return [
            'id',
            'name',
            'create',
            'update',
            'inn',
            'contracts',
            'modelName',
            'countProductPaid'
        ];
    }

    public function getModelName()
    {
        return  self::VIEW_TYPE;
    }

    public function getContracts ()
    {
        return $this->hasMany(Contract::class, ['personId' => 'id']);
    }

    public function getCreate ()
    {
        return strtotime($this->dateCreate);
    }

    public function getUpdate ()
    {
        return strtotime($this->dateUpdate);
    }

    public function getCustomer ()
    {
        return $this->hasOne(Customer::class, ['personId' => 'id']);
    }

    public function getCountProductPaid ()
    {
        return $this->customer->countProductPaid;
    }
}