<?php
namespace app\commands;

use yii\console\Controller;
use app\jobs\download\DownloadJob;

class JobsController extends Controller
{
    public function actionIndex()
    {
        \Yii::$app->queue->push(new DownloadJob());
    }
}