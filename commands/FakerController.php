<?php

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;

use app\models\Contract;
use app\models\Organisation;
use app\models\Person;
use app\models\Product;


class FakerController extends Controller
{
    function actionIndex()
    {
        for ($i = 0; $i <= 50; $i++) {
            $idPerson = self::addPerson();
            $idOrganisation = self::addOrganisation();
            for ($j = 0; $j <= rand(0, 10); $j++) {
                $idContractPerson = self::addContractPerson($idPerson);
                $idContractOrganisation = self::addContractOrganisation($idOrganisation);
                for ($k = 0; $k <= rand(0, 5); $k++) {
                    self::addProduct($idContractPerson);
                    self::addProduct($idContractOrganisation);
                }
            }
        }

        return ExitCode::OK;
    }

    private function addPerson()
    {
        $faker = \Faker\Factory::create('ru_RU');

        $person = new Person();
        $fio = explode(' ', $faker->name());
        $person->surname = $fio[0];
        $person->name = $fio[1];
        $person->patronymic = $fio[2];
        $person->dateCreate = $faker->date('Y-m-d H:i:s');
        $person->dateUpdate = date('Y-m-d H:i:s');
        $person->inn = $faker->numberBetween(5000000000, 10000000000);
        if ($person->save()){
            echo 'add person' . "\n";
        }else{
            echo 'error add person' . "\n";
        }

        $id = $person->getPrimaryKey();

        return $id;
    }

    private function addOrganisation()
    {
        $faker = \Faker\Factory::create('ru_RU');
        $organisation = new Organisation();
        $organisation->name = $faker->company;
        $organisation->dateCreate = $faker->date('Y-m-d H:i:s');
        $organisation->dateUpdate = date('Y-m-d H:i:s');
        $organisation->inn = $faker->numberBetween(500000000000, 1000000000000);
        if ($organisation->save()){
            echo 'add organisation' . "\n";
        }else{
            echo 'error add organisation' . "\n";
        }

        $id = $organisation->getPrimaryKey();

        return $id;
    }

    private function addContractOrganisation($idOrganisation)
    {
        $faker = \Faker\Factory::create('ru_RU');
        $contract = new Contract();
        $contract->organisationId = $idOrganisation;
        $contract->number = "AD" . "{$faker->unique()->randomNumber($nbDigits = 5)}" . "/" . "{$faker->unique()->randomNumber($nbDigits = 5)}";
        $contract->paid = $faker->boolean(70);
        if ($contract->save()){
            echo 'add contract organisation' . "\n";
        }else{
            echo 'error add contract organisation' . "\n";
        }

        $id = $contract->getPrimaryKey();

        return $id;
    }

    private function addContractPerson($idPerson)
    {
        $faker = \Faker\Factory::create('ru_RU');
        $contract = new Contract();
        $contract->personId = $idPerson;
        $contract->number = "AD" . "{$faker->unique()->randomNumber($nbDigits = 5)}" . "/" . "{$faker->unique()->randomNumber($nbDigits = 5)}";
        $contract->paid = $faker->boolean(70);
        if ($contract->save()){
            echo 'add contract person' . "\n";
        }else{
            echo 'error add contract person' . "\n";
        }
        $id = $contract->getPrimaryKey();

        return $id;
    }

    private function addProduct($id)
    {
        $faker = \Faker\Factory::create('ru_RU');
        $product = new Product();
        $product->contractId = $id;
        $product->nameProduct = $faker->text($faker->numberBetween(10, 20));
        if ($product->save()){
            echo 'add product' . "\n";
        }else{
            echo 'error add product' . "\n";
        }
    }
}