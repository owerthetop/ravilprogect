<?php

namespace app\controllers;

use app\models\Customer;
use app\models\Person;
use yii\rest\Controller;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

class CustomerController extends Controller
{
    public function actionIndex()
    {
        $limit = \Yii::$app->request->get('perPage', 10);
        $page = \Yii::$app->request->get('page', 0);
        $name = \Yii::$app->request->get('name');
        $number = \Yii::$app->request->get('contractNumber');
        $sort = \Yii::$app->request->get('sort', SORT_ASC);

        $offset = 10 * $page;

        $query = Customer::find();

        if (!is_null($name) && !empty($name)) {
            $query->where(['like', 'name', ['name' => $name]]);
        }
        if (!is_null($number) && !empty($number)) {
            $query->andWhere(['like', 'number', ['number' => 'AD' . $number]]);
        }
        if ($sort == 'desc') {
            $sort = SORT_DESC;
        }

        $result = $query->orderBy(['name' => $sort])
            ->limit($limit)
            ->offset($offset)
            ->all();

        return $result;
    }

    public function actionView($id)
    {
        $result = $query = Customer::find()->where(['id' => $id])->one();

        if (!$result) {
            throw new NotFoundHttpException("Customer not fount");
        }

        return $result;
    }

    public function actionUpdate($id)
    {
        $data = \Yii::$app->request->getBodyParams();
        $customer = Customer::findOne($id);
        $model = ($customer->person) ?? $customer->organisation;
        if (!isset($data['name']) || !is_string($data['name']) || strlen($data['name']) > 255){
            throw new BadRequestHttpException('Bad request');
        }
        $model->name = $data['name'];

        if ($model instanceof Person){

            if (!isset($data['patronymic']) || !is_string($data['patronymic']) || strlen($data['patronymic']) > 255){
                throw new BadRequestHttpException('Bad request');
            }
            $model->patronymic = $data['patronymic'];

            if (!isset($data['surname']) || !is_string($data['surname']) || strlen($data['surname']) > 255){
                throw new BadRequestHttpException('Bad request');
            }
            $model->name = $data['surname'];
        }
        if (
            !isset($data['inn'])
            || !is_string($data['inn'])
            || strlen($data['inn']) > 255
            || !preg_match('/^[0-9]+$/', $data['inn'])
        ){
            throw new BadRequestHttpException('Bad request');
        }
        $model->inn = (string)$data['inn'];
        $model->save();

        return $model;
    }
}