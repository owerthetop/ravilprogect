<?php
namespace app\controllers;

use yii\rest\ActiveController;

class ContractController extends ActiveController
{
    public $modelClass = 'app\models\Contract';

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['delete']);
        unset($actions['options']);

        return $actions;
    }
}