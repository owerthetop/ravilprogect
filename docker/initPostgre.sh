#!/bin/bash

POSTGRESQL_DATA=/var/www/docker/pgdata

if [ ! -d $POSTGRESQL_DATA ]; then
    mkdir -p $POSTGRESQL_DATA
    chown -R postgres:postgres $POSTGRESQL_DATA
    sudo -u postgres /usr/lib/postgresql/9.1/bin/initdb -D $POSTGRESQL_DATA -E 'UTF-8'
    ln -s /etc/ssl/certs/ssl-cert-snakeoil.pem $POSTGRESQL_DATA/server.crt
    ln -s /etc/ssl/private/ssl-cert-snakeoil.key $POSTGRESQL_DATA/server.key
    service postgresql start; \
	cd /; \
	su postgres -c "psql -c \"CREATE USER root WITH PASSWORD '123';\""; \
	su postgres -c "psql -c \"ALTER USER root WITH SUPERUSER;\""; \
	su postgres -c "psql -c \"CREATE DATABASE tk OWNER root;\""; \
	service postgresql stop
fi
