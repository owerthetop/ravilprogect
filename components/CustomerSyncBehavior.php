<?php

namespace app\components;

use app\models\Contract;
use app\models\Customer;
use app\models\Organisation;
use app\models\Person;
use app\models\Product;
use yii\base\Event;
use yii\db\ActiveRecord;
use yii\base\Behavior;

class CustomerSyncBehavior extends Behavior
{
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'addCustomer',
            ActiveRecord::EVENT_AFTER_UPDATE => 'updateCustomer'
        ];
    }

    /**
     * @param Event $event
     */
    public function addCustomer($event)
    {
        if ($event->sender instanceof Person) {
            $customer = new Customer();
            $customer->personId = $event->sender->id;
            $customer->name = $event->sender->name;
            $customer->save();
        } elseif ($event->sender instanceof Organisation) {
            $customer = new Customer();
            $customer->organisationId = $event->sender->id;
            $customer->name = $event->sender->name;
            $customer->save();
        } elseif ($event->sender instanceof Contract) {
            $contract = $event->sender;
            $customer = null;
            $customer = Customer::findOne(['organisationId' => $contract->organisationId, 'personId' => $contract->personId]);

            if (empty($customer->number) && $contract->paid == false) {
                $customer->number = $contract->number;
            } elseif (!empty($customer->number) && $contract->paid == false) {
                $customer->number .= ', ' . $contract->number;
            }
            $customer->save();
        } elseif ($event->sender instanceof Product) {
            $customer = null;
            $product = $event->sender;

            $contract = Contract::findOne($product->contractId);

            $customer = Customer::findOne(['organisationId' => $contract->organisationId, 'personId' => $contract->personId]);

            $countProduct = Product::find()
                ->join('INNER JOIN', 'contract', 'product."contractId" = contract.id')
                ->where([
                    Contract::tableName().'.paid' => true,
                    Contract::tableName().'.organisationId' => $contract->organisationId,
                    Contract::tableName().'.personId' => $contract->personId
                ])->count();

            if ($contract->paid == true){
                $customer->countProductPaid = $countProduct;
                $customer->save();
            }
        }
    }

    /**
     * @param $event
     */
    public function updateCustomer($event)
    {
        if ($event->sender instanceof Person) {
            $customer = Customer::find()->where(['personId'=> $event->sender->id])->one();
            $customer->name = $event->sender->name;
            $customer->update();
        } elseif ($event->sender instanceof Organisation) {
            $customer = Customer::find()->where(['organisationId' => $event->sender->id])->one();
            $customer->name = $event->sender->name;
            $customer->update();
        } elseif ($event->sender instanceof Contract) {
            $contract = $event->sender;
            $customer = null;
            $customer = Customer::findOne(['organisationId' => $contract->organisationId, 'personId' => $contract->personId]);

            if (empty($customer->number) && $contract->paid == false) {
                $customer->number = $contract->number;
            } elseif (!empty($customer->number) && $contract->paid == false) {
                $customer->number .= ', ' . $contract->number;
            }
            $customer->save();
        } elseif ($event->sender instanceof Product) {
            $customer = null;
            $product = $event->sender;

            $contract = Contract::findOne($product->contractId);

            $customer = Customer::findOne(['organisationId' => $contract->organisationId, 'personId' => $contract->personId]);

            $countProduct = Product::find()
                ->join('INNER JOIN', 'contract', 'product."contractId" = contract.id')
                ->where([
                    Contract::tableName().'.paid' => true,
                    Contract::tableName().'.organisationId' => $contract->organisationId,
                    Contract::tableName().'.personId' => $contract->personId
                ])->count();

            if ($contract->paid == true){
                $customer->countProductPaid = $countProduct;
                $customer->save();
            }
        }
    }
}